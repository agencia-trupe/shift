@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('icone', 'Ícone (150x150px)') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/consultorias/'.$registro->icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('icone', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('icone_hover', 'Ícone Hover') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/consultorias/'.$registro->icone_hover) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('icone_hover', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.consultorias.index') }}" class="btn btn-default btn-voltar">Voltar</a>
