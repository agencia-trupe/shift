@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Consultoria de Negócios /</small> Editar Consultoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.consultorias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.consultorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
