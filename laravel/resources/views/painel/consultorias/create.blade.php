@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Consultoria de Negócios /</small> Adicionar Consultoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.consultorias.store', 'files' => true]) !!}

        @include('painel.consultorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
