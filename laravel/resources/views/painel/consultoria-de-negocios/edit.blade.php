@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Consultoria de Negócios
            <a href="{{ route('painel.consultorias.index') }}" class="btn btn-sm btn-info pull-right">
                <span class="glyphicon glyphicon-align-left" style="margin-right:10px"></span>
                Editar Consultorias
            </a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.consultoria-de-negocios.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.consultoria-de-negocios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
