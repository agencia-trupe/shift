@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
                @if($banner->link)
                <a href="{{ Tools::parseLink($banner->link) }}" target="_blank" class="slide">
                @else
                <div class="slide">
                @endif

                    <div class="center">
                        <div class="background">
                            <img src="{{ asset('assets/img/banners/'.$banner->background) }}" alt="">
                        </div>

                        <div class="titulo">
                            <span>{!! $banner->titulo !!}</span>
                        </div>

                        <div class="box"></div>

                        <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="imagem" alt="">

                        <div class="descricao">{!! $banner->texto !!}</div>
                    </div>

                @if($banner->link)
                </a>
                @else
                </div>
                @endif
            @endforeach

            <div class="cycle-pager"></div>
        </div>

        <div class="center">
            <div class="consultorias-home">
                <div class="wrapper">
                    <div class="titulo">
                        <h2>CONSULTORIA DE NEGÓCIOS</h2>
                        <h3>{{ strip_tags($consultoriaTexto->titulo) }}</h3>
                    </div>
                    <div class="icones">
                        @foreach($consultorias as $consultoria)
                        <a href="{{ route('consultoria', ['sel' => $consultoria->slug]) }}">
                            <img src="{{ asset('assets/img/consultorias/'.$consultoria->icone) }}" alt="">
                            <span>{{ $consultoria->titulo }}</span>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
