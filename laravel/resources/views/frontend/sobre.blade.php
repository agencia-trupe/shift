@extends('frontend.common.template')

@section('content')

    <div class="conteudo sobre">
        <div class="center">
            <div class="imagem-wrapper">
                <div class="imagem">
                    <img src="{{ asset('assets/img/sobre/'.$sobre->imagem) }}" alt="">
                </div>
                <div class="titulo"><span>SOBRE NÓS</span></div>
            </div>
            <div class="texto-wrapper">
                <div class="texto">
                    {!! $sobre->texto !!}
                </div>
            </div>
        </div>

        <div class="direccao-estrategica">
            <div class="center">
                <h2>Direcção Estratégica</h2>
                <div class="icones">
                    <div class="icone icone-1">
                        <div class="circulo-1">
                            Criatividade<br>
                            e inovação
                        </div>
                        <div class="circulo-2"></div>
                    </div>
                    <div class="seta"></div>
                    <div class="icone icone-2">
                        <div class="circulo-1">
                            Parceria
                        </div>
                        <div class="circulo-2"></div>
                    </div>
                    <div class="seta"></div>
                    <div class="icone icone-3">
                        <div class="circulo-1">
                            Equipa<br>
                            Produtiva
                        </div>
                        <div class="circulo-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
