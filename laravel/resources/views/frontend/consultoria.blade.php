@extends('frontend.common.template')

@section('content')

    <div class="conteudo">
        <div class="center">
            <div class="imagem-wrapper">
                <div class="imagem">
                    <img src="{{ asset('assets/img/consultoria-de-negocios/'.$consultoriaTexto->imagem) }}" alt="">
                </div>
                <div class="titulo"><span>CONSULTORIA DE NEGÓCIOS</span></div>
            </div>
            <div class="texto-wrapper">
                <div class="texto">
                    <h2 style="font-weight:bold;font-style:italic">{!! $consultoriaTexto->titulo !!}</h2>
                    {!! $consultoriaTexto->texto !!}
                </div>
            </div>
        </div>

        <div class="consultorias">
            <div class="center">
                <div class="icones">
                    @foreach($consultorias as $c)
                    <a href="#" data-id="{{ $c->id }}" @if(request('sel') == $c->slug) class="active consultoria-scroll" @endif>
                        <div class="icone">
                            <img src="{{ asset('assets/img/consultorias/'.$c->icone) }}" alt="">
                            <img src="{{ asset('assets/img/consultorias/'.$c->icone_hover) }}" alt="">
                        </div>
                        <span>{{ $c->titulo }}</span>
                        <p class="descricao-mobile">{!! $c->descricao !!}</p>
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="descricao" @if(request('sel') && $consultorias->contains('slug', request('sel'))) style="display:block;" @endif>
                <div class="center">
                    @foreach($consultorias as $c)
                    <p data-id="{{ $c->id }}" @if(request('sel') && $consultorias->contains('slug', request('sel')) && $c->slug != request('sel')) style="display:none" @endif>{!! $c->descricao !!}</p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
