@extends('frontend.common.template')

@section('content')

    <div class="conteudo contato">
        <div class="center">
            <div class="imagem-wrapper">
                <div class="imagem">
                    <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                </div>
                <div class="titulo"><span>CONTATO</span></div>
            </div>
            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! strip_tags($contato->endereco, '<br>') !!}</p>
            </div>
            <div class="texto-wrapper">
                <form action="{{ route('contato.post') }}" method="POST">
                    {!! csrf_field() !!}

                    <p>Envie sua mensagem</p>

                    @if(session('enviado'))
                    <div class="flash enviado">Mensagem enviada com sucesso!</div>
                    @endif
                    @if($errors->any())
                    <div class="flash erro">Preencha todos os campos corretamente.</div>
                    @endif

                    <div class="input">
                        <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    </div>
                    <div class="input">
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    </div>
                    <div class="input">
                        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                    </div>
                    <div class="input">
                        <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                    </div>
                    <button>
                        <span>ENVIAR</span>
                    </button>
                </form>
            </div>
        </div>

        <div class="mapa">{!! $contato->google_maps !!}</div>
    </div>

@endsection
