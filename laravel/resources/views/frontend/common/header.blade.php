    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/img/layout/shift.png') }}" alt="">
            </a>
            <a href="{{ route('home') }}" class="slogan">
                <img src="{{ asset('assets/img/layout/slogan.png') }}" alt="">
            </a>
            <nav id="nav-desktop">
                <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre')) class="active" @endif>SOBRE NÓS</a>
                <div class="dropdown-handle">
                    <a href="javascript:void(0)" @if(Tools::routeIs(['servicos', 'consultoria'])) class="active" @endif>SERVIÇOS</a>
                    <div class="dropdown">
                        <div class="col">
                            @foreach($servicos as $slug => $titulo)
                            <a href="{{ route('servicos', $slug) }}">&raquo; {{ $titulo }}</a>
                            @endforeach
                        </div>
                        <div class="col">
                            <a href="{{ route('consultoria') }}" style="font-weight:bold">&raquo; CONSULTORIA DE NEGÓCIOS</a>
                            @foreach($consultorias as $slug => $titulo)
                            <a href="{{ route('consultoria', ['sel' => $slug]) }}">&raquo; {{ $titulo }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <a href="#" class="tooltip" data-pt-title="Em breve" data-pt-scheme="black" data-pt-position="bottom">EVENTOS</a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>CONTATO</a>
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
            <div class="contato-header">
                <p>
                    Telemovel: {{ $contato->telefone }}<br>
                    E-mail: <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                </p>
            </div>
        </div>
    </header>

    <div id="nav-mobile">
        <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre')) class="active" @endif>SOBRE NÓS</a>
        <a href="javascript:void(0)" @if(Tools::routeIs('servicos')) class="active" @endif>SERVIÇOS</a>
        <div class="sub">
            @foreach($servicos as $slug => $titulo)
                <a href="{{ route('servicos', $slug) }}">{{ $titulo }}</a>
            @endforeach
        </div>
        <a href="{{ route('consultoria') }}" @if(Tools::routeIs('consultoria')) class="active" @endif>CONSULTORIA DE NEGÓCIOS</a>
        <div class="sub">
            @foreach($consultorias as $slug => $titulo)
            <a href="{{ route('consultoria', ['sel' => $slug]) }}">{{ $titulo }}</a>
            @endforeach
        </div>
        <a href="#" class="tooltip" data-pt-title="Em breve" data-pt-scheme="black" data-pt-position="left" data-pt-size="small">EVENTOS</a>
        <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>CONTATO</a>
    </div>
