    <footer>
        <div class="center">
            <div class="informacoes">
                <img src="{{ asset('assets/img/layout/shift-footer.png') }}" alt="">
                <p>
                    Telemovel: <span>{{ $contato->telefone }}</span><br>
                    E-mail: <span><a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></span><br>
                    Endereço: <span>{{ strip_tags(str_replace('<br />', ' &middot; ', $contato->endereco)) }}</span>
                </p>
                <p>© {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados.</p>
            </div>
            <div class="links">
                <div class="col">
                    <a href="{{ route('home') }}">HOME</a>
                    <a href="{{ route('sobre') }}">SOBRE NÓS</a>
                    <a href="#" class="tooltip" data-pt-title="Em breve" data-pt-scheme="black" data-pt-size="small" data-pt-position="right">EVENTOS</a>
                    <a href="{{ route('contato') }}">CONTATO</a>
                </div>
                <div class="col">
                    <span class="titulo">SERVIÇOS</span>
                    <div class="sub">
                        @foreach($servicos as $slug => $titulo)
                        <a href="{{ route('servicos', $slug) }}">{{ $titulo }}</a>
                        @endforeach
                    </div>
                </div>
                <div class="col">
                    <a href="{{ route('consultoria') }}">CONSULTORIA DE NEGÓCIOS</a>
                    <div class="sub">
                        @foreach($consultorias as $slug => $titulo)
                        <a href="{{ route('consultoria', ['sel' => $slug]) }}">{{ $titulo }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </footer>
