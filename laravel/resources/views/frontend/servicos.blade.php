@extends('frontend.common.template')

@section('content')

    <div class="conteudo">
        <div class="center">
            <div class="imagem-wrapper">
                <div class="imagem">
                    <img src="{{ asset('assets/img/servicos/'.$servico->imagem) }}" alt="">
                </div>
                <div class="titulo"><span>{{ $servico->titulo }}</span></div>
            </div>
            <div class="texto-wrapper">
                <div class="texto">
                    {!! $servico->texto !!}
                </div>
            </div>
        </div>
    </div>

@endsection
