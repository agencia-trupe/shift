import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$.protip({selector: '.tooltip'});
$('.tooltip').click(function(e) {
    e.preventDefault();
});

$('.consultorias a').click(function(event) {
    event.preventDefault();

    if ($(this).hasClass('active')) return;

    var id = $(this).data('id');

    $('.consultorias a').removeClass('active');
    $(this).addClass('active');

    $('.descricao p').hide();
    $('.descricao p[data-id=' + id + ']').show();

    $('.descricao').show();
});

if ($('.consultoria-scroll').length) {
    var targetOffset = $('.consultorias .active').offset().top;
    $('html,body').animate({scrollTop: targetOffset});
}

$('.banners').cycle({
    slides: '>.slide',
    pager: '.cycle-pager'
});
