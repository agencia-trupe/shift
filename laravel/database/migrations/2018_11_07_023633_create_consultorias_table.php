<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultoriasTable extends Migration
{
    public function up()
    {
        Schema::create('consultorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->text('descricao');
            $table->string('icone');
            $table->string('icone_hover');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('consultorias');
    }
}
