<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Consultoria extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'consultorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_icone()
    {
        return CropImage::make('icone', [
            'width'  => 150,
            'height' => 150,
            'path'   => 'assets/img/consultorias/'
        ]);
    }

    public static function upload_icone_hover()
    {
        return CropImage::make('icone_hover', [
            'width'  => 150,
            'height' => 150,
            'path'   => 'assets/img/consultorias/'
        ]);
    }
}
