<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ConsultoriaDeNegocios extends Model
{
    protected $table = 'consultoria_de_negocios';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 620,
            'height' => 320,
            'path'   => 'assets/img/consultoria-de-negocios/'
        ]);
    }

}
