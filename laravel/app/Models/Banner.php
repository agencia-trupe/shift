<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_background()
    {
        return CropImage::make('background', [
            'width'  => 1600,
            'height' => 440,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 520,
            'height' => 310,
            'path'   => 'assets/img/banners/'
        ]);
    }
}
