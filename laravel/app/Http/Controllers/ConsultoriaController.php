<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ConsultoriaDeNegocios;
use App\Models\Consultoria;

class ConsultoriaController extends Controller
{
    public function index()
    {
        $consultoriaTexto = ConsultoriaDeNegocios::first();
        $consultorias     = Consultoria::ordenados()->get();

        return view('frontend.consultoria', compact('consultoriaTexto', 'consultorias'));
    }
}
