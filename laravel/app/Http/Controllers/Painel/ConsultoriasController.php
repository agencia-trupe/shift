<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConsultoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\Consultoria;

class ConsultoriasController extends Controller
{
    public function index()
    {
        $registros = Consultoria::ordenados()->get();

        return view('painel.consultorias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.consultorias.create');
    }

    public function store(ConsultoriasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Consultoria::upload_icone();

            if (isset($input['icone_hover'])) $input['icone_hover'] = Consultoria::upload_icone_hover();

            Consultoria::create($input);

            return redirect()->route('painel.consultorias.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Consultoria $registro)
    {
        return view('painel.consultorias.edit', compact('registro'));
    }

    public function update(ConsultoriasRequest $request, Consultoria $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Consultoria::upload_icone();

            if (isset($input['icone_hover'])) $input['icone_hover'] = Consultoria::upload_icone_hover();

            $registro->update($input);

            return redirect()->route('painel.consultorias.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Consultoria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.consultorias.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
