<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConsultoriaDeNegociosRequest;
use App\Http\Controllers\Controller;

use App\Models\ConsultoriaDeNegocios;

class ConsultoriaDeNegociosController extends Controller
{
    public function index()
    {
        $registro = ConsultoriaDeNegocios::first();

        return view('painel.consultoria-de-negocios.edit', compact('registro'));
    }

    public function update(ConsultoriaDeNegociosRequest $request, ConsultoriaDeNegocios $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ConsultoriaDeNegocios::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.consultoria-de-negocios.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
