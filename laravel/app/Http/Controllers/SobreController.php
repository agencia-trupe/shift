<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Sobre;

class SobreController extends Controller
{
    public function index()
    {
        $sobre = Sobre::first();

        return view('frontend.sobre', compact('sobre'));
    }
}
