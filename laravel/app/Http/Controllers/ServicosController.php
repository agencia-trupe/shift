<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servico;

class ServicosController extends Controller
{
    public function index(Servico $servico)
    {
        return view('frontend.servicos', compact('servico'));
    }
}
