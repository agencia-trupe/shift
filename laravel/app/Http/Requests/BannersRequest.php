<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'texto' => 'required',
            'background' => 'required|image',
            'imagem' => 'required|image',
            'link' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['background'] = 'image';
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
