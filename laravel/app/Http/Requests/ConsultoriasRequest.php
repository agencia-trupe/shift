<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConsultoriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'descricao' => 'required',
            'icone' => 'required|image',
            'icone_hover' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['icone'] = 'image';
            $rules['icone_hover'] = 'image';
        }

        return $rules;
    }
}
